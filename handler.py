import json
import urllib2

# this handles potential unicode eg Montreal
def byteify(input):
    if isinstance(input, dict):
        return {byteify(key): byteify(value)
                for key, value in input.iteritems()}
    elif isinstance(input, list):
        return [byteify(element) for element in input]
    elif isinstance(input, unicode):
        return input.encode('utf-8')
    else:
        return input

nhl_api_url = 'https://statsapi.web.nhl.com'

periodStrings = ['1st', '2nd','3rd', 'OT']
#def formatPeriod(period):



def handle(st):
    the_game = json.load(urllib2.urlopen(st))
    all_plays = the_game['liveData']['plays']['allPlays']
    for event in all_plays:
        try:
            result = event['result']
            if (result['event'] == 'Goal'):
                print 'Scoring update: {}'.format(result['description'])
                about = event['about']
                #print 'About is: {}'.format(about)
                print 'Game info: Away {}, Home {} ({} of {})'.format(about['goals']['away'],about['goals']['home'],about['periodTime'],periodStrings[int(about['period'])-1])
            elif result['event'] == 'Period End':
                about = event['about']
                print '{} period has ended, score is Away: {}, Home: {}'.format(periodStrings[int(about['period'])-1],about['goals']['away'],about['goals']['home'])
                print ''
            elif result['event'] == 'Period Start':
                about = event['about']
                print '{} period started'.format(periodStrings[int(about['period']) - 1])

            #print 'Result is: {}'.format(event['about'])
        except KeyError:
            # not a goal
            pass


    #print(the_game['liveData']['plays']['allPlays'])
if __name__ == "__main__":
    handle('https://statsapi.web.nhl.com/api/v1/game/2017020063/feed/live')